
			      RECITATOR
			    -------------
			    
Recitator is a set of scripts that sets BROWSER, HTTP(S)_PROXY and
http(s)_proxy environment variables

- pasco.sh : bash script to set a BROWSER environment variable

- vicarius.sh :
  bash script that sets environment variables
  HTTP_PROXY, HTTPS_PROXY, http_proxy and https_proxy
  after testing

- get-free-proxy-list.pl :
  Perl script that fetches a list of elite proxies from
  free-proxy-list.net
