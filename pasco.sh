#!/bin/bash

# pasco.sh
# Mar 21, 2019
# sets an environment variable called BROWSER
# This environment variable is the user agent string of popular browsers
# List from https://techblog.willshouse.com/2012/01/03/most-common-user-agents/

# Code refs and acknowledgements
# https://stackoverflow.com/questions/1194882/how-to-generate-random-number-in-bash
# ref https://www.cyberciti.biz/faq/finding-bash-shell-array-length-elements
#declare -a ua
readarray ua < pasco-strings
#UA='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
UA=${ua[((RANDOM % ${#ua[@]} - 1))]}

#echo "$UA"
set -a
# Thanks to https://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
BROWSER="$(echo ${UA} | sed -e 's/[[:space:]]*$//')"
set +a

if [[ "$DEBUG" -gt 0 ]]; then
    echo "BROWSER is $BROWSER"
fi
