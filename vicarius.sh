#!/bin/bash

# vicarius.sh
# Mar 21, 2019
# sets environment variables HTTP(S)_PROXY and http(s)_proxy

# Code refs and acknowledgements
# https://stackoverflow.com/questions/1194882/how-to-generate-random-number-in-bash
# ref https://www.cyberciti.biz/faq/finding-bash-shell-array-length-elements
# get_working_proxy
# cycles through free proxies and stops at the first working proxy
# - repeatedly calls random_free_proxy
#   - for each proxy, we try to GET whatsmyip
#      - match: return this proxy as working proxy
#      - no match: next get_free_proxy

PROXY_FOUND="FALSE"
proxy=""

# unset the proxy variables
unset HTTP_PROXY
unset HTTPS_PROXY
unset http_proxy
unset https_proxy

# choose to be a random browser
. ./pasco.sh
if [[ "$DEBUG" -gt 0 ]];then
    echo -e "User agent is $BROWSER"
fi

# an array of destinations to test if a proxy is working
# these websites return the client IP.
# we randomize the elements
if [ -f vicarius-destinations ]; then
    readarray dests < vicarius-destinations
    numDests=${#dests[@]}
    for index in ${!dests[@]}
    do
	randomIndex=$(( RANDOM % $numDests ))
	tmp="${dests[$randomIndex]}"
	dests[$randomIndex]="${dests[(( $numDests - 1 ))]}"
	dests[(( $numDests - 1 ))]=$tmp
    done
fi

# randomize proxy list exist
if [ -f vicarius-proxies ]
then
    
    # if proxy list is not empty
    numProxies=$(wc -l vicarius-proxies | awk '{print $1}')
    #echo "Number of proxies is " $numProxies
    if [ $numProxies -gt 0 ]
    then
	
	# create an array of proxies from which a random proxy will be chosen
	readarray proxies < vicarius-proxies # will work only if bash > v.4
	#echo "Number of elements in proxies is " ((${#proxies[@]} - 1))
	
	# now that proxies is an actual array, we need to randomize the elements
	# the crude hack here is to swap random elements with the last element
	# and do this swap as many times as the length of the proxies array
	# we are probably living in a state of sin !
	numProxies=${#proxies[@]}
	for index in ${!proxies[@]}; do
	    randomIndex=$(( RANDOM % $numProxies ))
	    tmp="${proxies[$randomIndex]}"
	    proxies[$randomIndex]="${proxies[(( $numProxies - 1 ))]}"
	    proxies[(( $numProxies - 1 ))]=$tmp
	done
    fi
fi

# iterate through the proxies array looking for a proxy that works
# break prematurely if such a proxy is found.
# if no working proxy is found, we will unset all proxy variables
for proxy in ${proxies[@]}; do

    if [[ "$DEBUG" -gt 0 ]];then    
	echo Proxy is $proxy
    fi
    
    ip=$(echo $proxy | awk -F: '{print $1}')
    
    tmpfile=$(mktemp)
	    
    # iterate through the destination array
    for dest in ${dests[@]}; do

	if [[ "$DEBUG" -gt 0 ]];then
	    echo "Destination is $dest"
	fi
	
	# the test
	if [[ "$DEBUG" -gt 0 ]];then
	    HTTP_PROXY=http://$proxy HTTPS_PROXY=http://$proxy http_proxy=http://$proxy https_proxy=http://$proxy curl -v -s -A "$BROWSER" "$dest" > $tmpfile
	else
	    HTTP_PROXY=http://$proxy HTTPS_PROXY=http://$proxy http_proxy=http://$proxy https_proxy=http://$proxy curl -s -A "$BROWSER" "$dest" > $tmpfile
	fi
	
	grep -q "$ip" $tmpfile && grep -ivq "PROXY" $tmpfile
	retval=$?
	#echo Ret Val is $retval
	if [[ $retval -eq 0 ]]; then # test succeeded, so we export proxy variables
	    set -a
	    HTTP_PROXY="http://$proxy"
	    HTTPS_PROXY="http://$proxy"
	    http_proxy="http://$proxy"
	    https_proxy="http://$proxy"
	    set +a
	    PROXY_FOUND="TRUE"
	    if [[ "$DEBUG" -gt 0 ]];then
		echo Proxy found is $PROXY_FOUND
	    fi
	    rm -f $tmpfile
	    break	 
	fi
	
	if [ "$PROXY_FOUND" = "TRUE" ]; then
	    break
	else # test failed : get next proxy
	    	if [[ "$DEBUG" -gt 0 ]];then
		    echo "Proxy found is $PROXY_FOUND"
		    echo "Looking for next dest"
		fi
	    continue
	fi
	
    done # get next dest
    
    if [ "$PROXY_FOUND" = "TRUE" ]; then
	break
    else # test failed : get next proxy
	if [[ "$DEBUG" -gt 0 ]];then
	    echo "Proxy found is $PROXY_FOUND"
	    echo "Looking for next proxy"
	    echo
	fi
	continue
    fi
done

# if no working proxy is found, we explicitly unset proxy variables
if [ "$PROXY_FOUND" = "FALSE" ]; then
    
    # unset the proxy variables
    unset HTTP_PROXY
    unset HTTPS_PROXY
    unset http_proxy
    unset https_proxy
    if [[ "$DEBUG" -gt 0 ]];then
	echo "No Proxy found"
    fi
fi
